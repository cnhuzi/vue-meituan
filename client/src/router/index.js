import { createRouter, createWebHashHistory } from 'vue-router'


const routes = [
  {
    path: '/',
    redirect: '/login'
  },
  {
    path: '/login',
    name: 'login',
    component: () => import('@/views/Login.vue'),
    meta: {
      index:1
    }
  },
  {
    path: '/home',
    name: 'home',
    component: () => import('@/views/Home.vue'),
    meta: {
      index:1
    }
  },
  {
    path: '/member',
    name: 'member',
    component: () => import('@/views/Member.vue'),
    meta: {
      index:1
    }
  },
  {
    path: '/order',
    name: 'order',
    component: () => import('@/views/Order.vue'),
    meta: {
      index:1
    }
  },
  {
    path: '/own',
    name: 'own',
    component: () => import('@/views/Own.vue'),
    meta: {
      index:1
    }
  },
  {
    path: '/detail/:id',
    name: 'detail',
    component: () => import('@/views/Detail.vue'),
    meta: {
      index:1,
    },
  },
  {
    path: '/itemdetail/:id',
    name: 'itemdetail',
    component: () => import('@/views/Itemdetail.vue'),
    meta: {
      index:1
    }
  },
]

const router = createRouter({
  history: createWebHashHistory(process.env.BASE_URL),
  routes
})

export default router
