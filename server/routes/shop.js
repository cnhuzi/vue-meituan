const router = require('koa-router')()
const userService = require('../controllers/mySqlConfig')
router.prefix('/shop')

// 定义获取商店接口
router.get('/shopitem', async (ctx, next) => {
    await userService.getshoplist()
        .then(res => {
            for (let item of res) {
                item.shopmark = JSON.parse(item.shopmark)
                item.type = JSON.parse(item.type)
            }
            ctx.body = {
                code: 200,
                mess: res
            }
        })
        .catch(err => {
            console.log(err);
            ctx.body = {
                code: '80002',
                data: err
            }
        })
})

// 获取商店详情商品接口
router.post('/itemlist', async (ctx, next) => {
    let _shopid = ctx.request.body.shopid
    await userService.detaillist(_shopid).then(res => {
        ctx.body = {
            code: 200,
            mess: res
        }
    })
        .catch(err => {
            console.log(err);
            ctx.body = {
                code: '80002',
                data: err
            }
        })
})

// 获取需要的商品接口
router.post('/classifyitem', async (ctx, next) => {
    let _shopid = ctx.request.body.shopid
    await userService.classifyitem(_shopid).then(res => {
        for(let item of res) {
            item.classifyitem = JSON.parse(item.classifyitem)
        }
        ctx.body = {
            code: 200,
            mess: res
        }
    })
        .catch(err => {
            console.log(err);
            ctx.body = {
                code: '80002',
                data: err
            }
        })
})
router.post('/comment', async (ctx, next) => {
    let _shopid = ctx.request.body.shopid
    await userService.getcomment(_shopid).then(res => {
        ctx.body = {
            code: 200,
            mess: res
        }
    }).catch(err => {
        ctx.body = {
            code: '80002',
            data: err
        }
    })
})
module.exports = router