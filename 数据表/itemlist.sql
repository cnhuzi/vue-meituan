-- phpMyAdmin SQL Dump
-- version 4.4.15.10
-- https://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: 2022-05-10 15:44:28
-- 服务器版本： 8.0.24
-- PHP Version: 5.6.40

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `meituan`
--

-- --------------------------------------------------------

--
-- 表的结构 `itemlist`
--

CREATE TABLE IF NOT EXISTS `itemlist` (
  `itemid` int NOT NULL,
  `itemimg` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL,
  `itemname` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL,
  `itemsellnum` int DEFAULT NULL,
  `itemprice` float DEFAULT NULL,
  `itemcmt` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL,
  `controduce` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL,
  `shoptype` int DEFAULT NULL,
  `_id` int NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=37 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci ROW_FORMAT=DYNAMIC;

--
-- 转存表中的数据 `itemlist`
--

INSERT INTO `itemlist` (`itemid`, `itemimg`, `itemname`, `itemsellnum`, `itemprice`, `itemcmt`, `controduce`, `shoptype`, `_id`) VALUES
(2, 'https://meituanoyjy.oss-cn-hangzhou.aliyuncs.com/detailimg3.png.PNG\n', '牛骨粉', 409, 18.8, '', '陆敏', 1, 1),
(3, 'https://meituanoyjy.oss-cn-hangzhou.aliyuncs.com/detailimg1.png.PNG\n', '卤猪脚', 80, 16.8, '', '邓敏', 1, 2),
(4, 'https://meituanoyjy.oss-cn-hangzhou.aliyuncs.com/detailimg5.png.PNG\n', '牛骨面', 54, 18.8, '好评度80%', '孙芳', 1, 3),
(5, 'https://meituanoyjy.oss-cn-hangzhou.aliyuncs.com/detailimg6.png.PNG\n', '烤生蚝', 64, 10, '', '许静', 1, 4),
(6, 'https://meituanoyjy.oss-cn-hangzhou.aliyuncs.com/detailimg11.png.PNG\n', '烤小羊腿', 6, 68, '', '潘洋', 1, 5),
(7, 'https://meituanoyjy.oss-cn-hangzhou.aliyuncs.com/detailimg12.png.PNG\n', '烤羊排', 12, 58.8, '', '萧强', 1, 6),
(8, 'https://meituanoyjy.oss-cn-hangzhou.aliyuncs.com/detailimg13.png.PNG\n', '纯牛杂', 5, 18.8, '', '郭勇', 1, 7),
(9, 'https://meituanoyjy.oss-cn-hangzhou.aliyuncs.com/detailimg14.png.PNG\n', '卤牛杂面', 3, 18, '', '萧磊', 1, 8),
(10, 'https://meituanoyjy.oss-cn-hangzhou.aliyuncs.com/detailimg15.png.PNG\n', '牛骨加雪碧', 48, 37.8, '', '阎桂英', 1, 9),
(11, 'https://meituanoyjy.oss-cn-hangzhou.aliyuncs.com/detailimg16.png.PNG\n', '牛骨加可乐', 3, 22, '', '徐敏', 1, 10),
(12, 'https://meituanoyjy.oss-cn-hangzhou.aliyuncs.com/detailimg17.png.PNG\n', '牛骨加冰红茶', 4, 23, '', '龙霞', 1, 11),
(13, 'https://meituanoyjy.oss-cn-hangzhou.aliyuncs.com/detailimg18.png.PNG\n', '牛骨加粉', 4, 39.9, '好评度55%', '李娟', 1, 12),
(14, 'https://meituanoyjy.oss-cn-hangzhou.aliyuncs.com/IMG_0175.PNG\n', '猪脚饭', 6, 18.8, '', '高霞', 1, 13),
(15, 'https://meituanoyjy.oss-cn-hangzhou.aliyuncs.com/IMG_0176.PNG\n', '牛杂饭', 1, 18.8, '', '程娜', 1, 14),
(16, 'https://meituanoyjy.oss-cn-hangzhou.aliyuncs.com/detailimg20.png.PNG\n', '菜饭分开', 4, 1, '', '白丽', 1, 15),
(17, 'https://meituanoyjy.oss-cn-hangzhou.aliyuncs.com/IMG_0177.PNG\n', '烤鸡爪', 70, 3, '', '萧强', 1, 16),
(18, 'https://meituanoyjy.oss-cn-hangzhou.aliyuncs.com/detailimg23.png.PNG\n', '烤鸭脖', 12, 6, '', '傅娜', 1, 17),
(19, 'https://meituanoyjy.oss-cn-hangzhou.aliyuncs.com/detailimg24.png.PNG\n', '打包碗', 3, 2, '', '姚静', 1, 18),
(20, 'https://meituanoyjy.oss-cn-hangzhou.aliyuncs.com/detailimg25.png.PNG\n', '打包盒', 2, 1, '', '杨超', 1, 19),
(21, 'https://meituanoyjy.oss-cn-hangzhou.aliyuncs.com/detailimg27.png.PNG\n', '卤鸡爪', 8, 3, '', '姜刚', 1, 20),
(22, 'https://meituanoyjy.oss-cn-hangzhou.aliyuncs.com/detailimg10.png.PNG\n', '卤鸭脖', 7, 6, '好评度95%', '龙娜', 1, 21),
(23, 'https://meituanoyjy.oss-cn-hangzhou.aliyuncs.com/detailimg8.png.PNG\n', '纯牛骨', 30, 20, '', '杨秀英', 1, 22),
(24, 'https://meituanoyjy.oss-cn-hangzhou.aliyuncs.com/detailimg28.png.PNG\n', '百事', 17, 3, '', '吴军', 1, 23),
(25, 'https://meituanoyjy.oss-cn-hangzhou.aliyuncs.com/detailimg29.png.PNG\n', '南昌8度', 15, 6, '', '郭明', 1, 24),
(26, 'https://meituanoyjy.oss-cn-hangzhou.aliyuncs.com/detailimg30.png.PNG\n', '怡宝', 9, 3, '', '卢秀英', 1, 25),
(27, 'https://meituanoyjy.oss-cn-hangzhou.aliyuncs.com/detailimg31.png.PNG\n', '加多宝', 4, 4, '', '叶丽', 1, 26),
(28, 'https://meituanoyjy.oss-cn-hangzhou.aliyuncs.com/detailimg32.png.PNG\n', '江小白', 1, 18, '', '梁超', 1, 27),
(29, 'https://meituanoyjy.oss-cn-hangzhou.aliyuncs.com/detailimg33.png.PNG\n', '小郎酒', 1, 22, '', '郭艳', 1, 28),
(30, 'https://meituanoyjy.oss-cn-hangzhou.aliyuncs.com/detailimg34.png.PNG\n', '茉莉花茶', 3, 4, '', '杜明', 1, 29),
(31, 'https://meituanoyjy.oss-cn-hangzhou.aliyuncs.com/detailimg35.png.PNG\n', '康师傅冰红茶', 1, 4, '', '康平', 1, 30),
(32, 'https://meituanoyjy.oss-cn-hangzhou.aliyuncs.com/detailimg2.png.PNG\n', '花生米', 3, 7, '', '龚勇', 1, 31),
(33, 'https://meituanoyjy.oss-cn-hangzhou.aliyuncs.com/detailimg36.png.PNG\n', '蒜头', 10, 3, '', '侯刚', 1, 32),
(1, 'https://meituanoyjy.oss-cn-hangzhou.aliyuncs.com/detailimg4.png.PNG\n', '烤猪蹄', 408, 16.8, '好评度75%', '根据秘制配方研制,先卤后煮,皮脆肉嫩,焦香爽口,老嫩适度,老少皆宜!猪脚富含胶原蛋白,长期食用可提高皮肤弹性,是你佐餐,下酒,聚会的不二之选!', 1, 33),
(1, 'https://meituanoyjy.oss-cn-hangzhou.aliyuncs.com/IMG_0178.PNG\n', '香辣香饼', 292, 3, '好评度80%', '好吃', 2, 34),
(1, 'https://meituanoyjy.oss-cn-hangzhou.aliyuncs.com/IMG_9836.PNG', '鲍鱼饭', 546, 26, '好评度80%', '哈哈', 3, 35),
(1, 'https://meituanoyjy.oss-cn-hangzhou.aliyuncs.com/IMG_9837.PNG', '招牌卤味', 129, 19.9, '后面没数据啦', '这是最后介绍，最后一条数据', 4, 36);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `itemlist`
--
ALTER TABLE `itemlist`
  ADD PRIMARY KEY (`_id`) USING BTREE;

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `itemlist`
--
ALTER TABLE `itemlist`
  MODIFY `_id` int NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=37;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
