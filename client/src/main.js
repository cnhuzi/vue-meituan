import { createApp } from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'

import { Stepper, Sidebar, SidebarItem, Swipe, SwipeItem, Sticky, Tab, Tabs, Col, Row, Card, List, Search, Popup, Picker, Tabbar, TabbarItem, NavBar, Icon, Form, Field, CellGroup, Button, Loading } from 'vant';

const app = createApp(App)
app.use(Stepper)
app.use(Loading)
app.use(Sidebar);
app.use(SidebarItem);
app.use(Swipe);
app.use(SwipeItem);
app.use(Sticky)
app.use(Tab);
app.use(Tabs);
app.use(Col);
app.use(Row);
app.use(Card)
app.use(List)
app.use(Search)
app.use(Popup)
app.use(Picker)
app.use(Tabbar);
app.use(TabbarItem);
app.use(Button)
app.use(Form);
app.use(Field);
app.use(CellGroup);
app.use(Icon)
app.use(NavBar)
app.use(store)
app.use(router)
app.mount('#app')
