-- phpMyAdmin SQL Dump
-- version 4.4.15.10
-- https://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: 2022-05-10 15:44:49
-- 服务器版本： 8.0.24
-- PHP Version: 5.6.40

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `meituan`
--

-- --------------------------------------------------------

--
-- 表的结构 `shopitem`
--

CREATE TABLE IF NOT EXISTS `shopitem` (
  `shoptype` int NOT NULL,
  `shopname` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL,
  `shopgrade` float(255,1) DEFAULT NULL,
  `monthsell` bigint DEFAULT NULL,
  `personprice` int DEFAULT NULL,
  `startfee` int DEFAULT NULL,
  `truefee` int DEFAULT NULL,
  `selltime` int DEFAULT NULL,
  `shoprange` float DEFAULT NULL,
  `shopmark` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL,
  `type` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL,
  `backgroundImg` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL,
  `shopImg` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `public` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL,
  `look` float(255,1) DEFAULT NULL,
  `test` float(255,1) DEFAULT NULL,
  `manyi` int DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci ROW_FORMAT=DYNAMIC;

--
-- 转存表中的数据 `shopitem`
--

INSERT INTO `shopitem` (`shoptype`, `shopname`, `shopgrade`, `monthsell`, `personprice`, `startfee`, `truefee`, `selltime`, `shoprange`, `shopmark`, `type`, `backgroundImg`, `shopImg`, `public`, `look`, `test`, `manyi`) VALUES
(1, '金开猪脚王', 4.7, 671, 22, 15, 3, 30, 2.4, '{"gift":0,"drease":1,"member":1,"store":0}', '[{"shopadvantage":"近30天91人复购"},{"shopadvantage":"点评收录6年"}]', 'https://meituanoyjy.oss-cn-hangzhou.aliyuncs.com/IMG_9828.PNG', 'https://meituanoyjy.oss-cn-hangzhou.aliyuncs.com/shop.png', '新店开业，优惠多多哦', 4.6, 4.5, 94),
(2, '南京酱香饼(陕西凉皮凉面)', 3.9, 351, 9, 8, 1, 45, 0.5, '{"gift":0,"drease":0,"member":1,"store":0}', '[{"shopadvantage":"热情掌柜"},{"shopadvantage":"酱香饼美味可口"}]', 'https://meituanoyjy.oss-cn-hangzhou.aliyuncs.com/IMG_9834.PNG', 'https://meituanoyjy.oss-cn-hangzhou.aliyuncs.com/0CC6FFEEC5DE5C02598F157CA3C9AC03.png', '优惠多多', 4.5, 4.8, 89),
(3, '上隐食补.鲍鱼饭', 4.5, 1172, 37, 0, 10, 70, 9.6, '{"gift":0,"drease":0,"member":0,"store":0}', '[]', 'https://meituanoyjy.oss-cn-hangzhou.aliyuncs.com/IMG_9836.PNG', 'https://meituanoyjy.oss-cn-hangzhou.aliyuncs.com/91B1874C5A3A70985B9757D4BF1884DF.png', '欢饮光临', 4.4, 4.3, 79),
(4, '盛香亭热卤', 5.0, 796, 12, 20, 0, 35, 3.8, '{"gift":0,"drease":1,"member":1,"store":1}', '[{"shopadvantage":"精选好店"},{"shopadvantage":"近30天117人复购"}]', 'https://meituanoyjy.oss-cn-hangzhou.aliyuncs.com/IMG_9837.PNG', 'https://meituanoyjy.oss-cn-hangzhou.aliyuncs.com/E9838E6B1DEFFE1BD6C59EF41F8E87E3.png', '妈妈菜馆欢迎你', 3.9, 4.4, 98),
(5, '吉客周黄焖鸡米饭', 4.9, 2620, 12, 10, 1, 35, 0.54, '{"gift":0,"drease":0,"member":0,"store":1}', '[]', 'https://meituanoyjy.oss-cn-hangzhou.aliyuncs.com/IMG_9838.PNG', 'https://meituanoyjy.oss-cn-hangzhou.aliyuncs.com/FCE4B668D9989E42AF91FFD94863102A.png', '一串三', 4.7, 4.2, 65),
(6, '川香牛肉面馆', 5.0, 1908, 9, 10, 0, 40, 0.26, '{"gift":0,"drease":0,"member":0,"store":0}', '[{"shopadvantage":"热情掌柜"},{"shopadvantage":"点评收录3年"}]', 'https://meituanoyjy.oss-cn-hangzhou.aliyuncs.com/IMG_9839.PNG', 'https://meituanoyjy.oss-cn-hangzhou.aliyuncs.com/03ACC58911B0B358CD25807A818C760B.png', '三缺一', 4.7, 3.6, 85),
(7, '三叔公杂酱面馆', 4.8, 1892, 7, 0, 0, 42, 0.13, '{"gift":1,"drease":0,"member":0,"store":0}', '[{"shopadvantage":"门店上新"},{"shopadvantage":"热情掌柜"}]', 'https://meituanoyjy.oss-cn-hangzhou.aliyuncs.com/IMG_9840.PNG', 'https://meituanoyjy.oss-cn-hangzhou.aliyuncs.com/21E348284A6011120A8FE62C2D3E0422.png', '好吃不贵', 4.8, 4.8, 99),
(8, '黄蜀狼鸡公煲', 4.3, 153, 14, 10, 0, 64, 0.26, '{"gift":0,"drease":1,"member":0,"store":1}', '[{"shopadvantage":"热情掌柜"}]', 'https://meituanoyjy.oss-cn-hangzhou.aliyuncs.com/IMG_9841.PNG', 'https://meituanoyjy.oss-cn-hangzhou.aliyuncs.com/22B9CC3AA249EA1A1A64CCFAA9E8E850.png', '干净又卫生', 5.0, 5.0, 99),
(9, '金牌煲仔饭', 5.0, 2093, 18, 15, 0, 47, 0.1, '{"gift":0,"drease":0,"member":1,"store":1}', '[{"shopadvantage":"热情掌柜"}]', 'https://meituanoyjy.oss-cn-hangzhou.aliyuncs.com/IMG_9842.PNG', 'https://meituanoyjy.oss-cn-hangzhou.aliyuncs.com/374BB6188AEEC043360F91B5DB4947E0.png', '这是公告', 2.9, 4.2, 92),
(10, '柳星螺狮粉', 4.2, 1324, 11, 10, 0, 35, 0.14, '{"gift":1,"drease":0,"member":0,"store":0}', '[{"shopadvantage":"热情掌柜"}]', 'https://meituanoyjy.oss-cn-hangzhou.aliyuncs.com/IMG_9843.PNG', 'https://meituanoyjy.oss-cn-hangzhou.aliyuncs.com/4F7891DD70987BB7A046958CC1945CC9.png', '哈哈哈哈', 4.2, 4.3, 90),
(11, '湘味小碗菜', 4.5, 671, 22, 15, 3, 23, 1.1, '{"gift":0,"drease":1,"member":1,"store":0}', '[{"shopadvantage":"近30天91人复购"},{"shopadvantage":"点评收录6年"}]', 'https://meituanoyjy.oss-cn-hangzhou.aliyuncs.com/IMG_0188.PNG\n', 'https://meituanoyjy.oss-cn-hangzhou.aliyuncs.com/shop.png', '新店开业', 4.3, 4.5, 94),
(12, '特色蛋包饭', 35.0, 351, 9, 8, 1, 56, 0.5, '{"gift":0,"drease":0,"member":1,"store":0}', '[{"shopadvantage":"热情掌柜"},{"shopadvantage":"酱香饼美味可口"}]', 'https://meituanoyjy.oss-cn-hangzhou.aliyuncs.com/IMG_0189.PNG\n', 'https://meituanoyjy.oss-cn-hangzhou.aliyuncs.com/shop.png', '怎么说', 4.7, 4.8, 89),
(13, '台湾饭团', 4.5, 1172, 37, 0, 10, 33, 4.3, '{"gift":0,"drease":0,"member":0,"store":0}', '[]', 'https://meituanoyjy.oss-cn-hangzhou.aliyuncs.com/IMG_0190.PNG\n', 'https://meituanoyjy.oss-cn-hangzhou.aliyuncs.com/shop.png', 'wwww', 4.4, 4.3, 79),
(14, '合利家', 5.0, 796, 12, 20, 0, 20, 3.8, '{"gift":0,"drease":1,"member":1,"store":1}', '[{"shopadvantage":"精选好店"},{"shopadvantage":"近30天117人复购"}]', 'https://meituanoyjy.oss-cn-hangzhou.aliyuncs.com/IMG_0191.PNG\n', 'https://meituanoyjy.oss-cn-hangzhou.aliyuncs.com/shop.png', '家的味道', 4.6, 4.4, 98),
(15, '一品轩麻辣香锅', 4.9, 2620, 12, 10, 1, 35, 0.54, '{"gift":0,"drease":0,"member":0,"store":1}', '[]', 'https://meituanoyjy.oss-cn-hangzhou.aliyuncs.com/IMG_0192.PNG\n', 'https://meituanoyjy.oss-cn-hangzhou.aliyuncs.com/shop.png', '宜将剩勇追穷寇', 4.7, 4.2, 65),
(16, '吉米时代', 5.0, 1908, 9, 10, 0, 25, 0.26, '{"gift":0,"drease":0,"member":0,"store":0}', '[{"shopadvantage":"热情掌柜"},{"shopadvantage":"点评收录3年"}]', 'https://meituanoyjy.oss-cn-hangzhou.aliyuncs.com/IMG_0193.PNG\n', 'https://meituanoyjy.oss-cn-hangzhou.aliyuncs.com/shop.png', '不可沽名学霸王', 4.7, 3.6, 85),
(17, '重庆小面', 4.8, 1892, 7, 0, 0, 60, 2.6, '{"gift":1,"drease":0,"member":0,"store":0}', '[{"shopadvantage":"门店上新"},{"shopadvantage":"热情掌柜"}]', 'https://meituanoyjy.oss-cn-hangzhou.aliyuncs.com/IMG_0197.PNG\n', 'https://meituanoyjy.oss-cn-hangzhou.aliyuncs.com/shop.png', '这是条数据', 4.8, 4.8, 99),
(18, '西安名吃', 4.3, 153, 14, 10, 0, 15, 0.26, '{"gift":0,"drease":1,"member":0,"store":1}', '[{"shopadvantage":"热情掌柜"}]', 'https://meituanoyjy.oss-cn-hangzhou.aliyuncs.com/IMG_0194.PNG\n', 'https://meituanoyjy.oss-cn-hangzhou.aliyuncs.com/shop.png', '随便乱写的', 5.0, 5.0, 99),
(19, '砂锅粥·养生粥', 4.3, 2093, 18, 15, 0, 39, 5.4, '{"gift":1,"drease":0,"member":1,"store":1}', '[{"shopadvantage":"热情掌柜"}]', 'https://meituanoyjy.oss-cn-hangzhou.aliyuncs.com/IMG_0195.PNG\n', 'https://meituanoyjy.oss-cn-hangzhou.aliyuncs.com/shop.png', '数据太多了', 4.9, 4.2, 92),
(20, '小八仙牛肉饭', 4.2, 1324, 11, 10, 0, 44, 0.14, '{"gift":1,"drease":1,"member":0,"store":0}', '[{"shopadvantage":"热情掌柜"}]', 'https://meituanoyjy.oss-cn-hangzhou.aliyuncs.com/IMG_0196.PNG\n', 'https://meituanoyjy.oss-cn-hangzhou.aliyuncs.com/shop.png', '完成了', 4.9, 4.3, 90);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `shopitem`
--
ALTER TABLE `shopitem`
  ADD PRIMARY KEY (`shoptype`) USING BTREE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
