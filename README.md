# Vue-meituan

#### 介绍
Vue3+Vuex+koa美团外卖全栈项目 线上地址：http://114.132.234.68/

#### 软件架构
├─api // 接口封装管理 ├─assets // 字体配置及全局样式 ├─common // localStorage封装 ├─components // 可复用的组件 ├─views // 页面 ├─router // 路由配置文件 └─store //vuex 相关文件 App.vue // 根组件 main.js // 入口文件

#### 数据
需要数据的可以在数据表文件中找到相应的sql文件，需要的自取，记得star哟