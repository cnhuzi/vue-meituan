import axios from 'axios'

axios.defaults.baseURL = "http://114.132.234.68:8889";


export default function ajax(url="", data={}, type="GET") {
    return new Promise((resolve, reject) => {
        let promise;
        if(type === 'GET') {
            promise = axios.get(url, {
                params: data
            })
        } else {
            promise = axios.post(url, data)
        }

        promise.then(val => {
            resolve(val.data)
        }).catch(err => {
            reject(err)
            console.log('请求出错了');
        })
    })
}