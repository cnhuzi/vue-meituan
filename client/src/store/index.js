import { createStore } from 'vuex'

export default createStore({
  state: {
    totalPrice: 0, // 购买的总价格
    cartItem: [], // 购买的商品卡片
    num: 0,
    comment: [],
    flag: Boolean,
    itemlist: [],
    shopdetail: {},
    classify: [],
    user: {},
  },
  mutations: {
    },
  actions: {
  },
  modules: {
  }
})
