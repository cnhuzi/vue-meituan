import ajax from './ajax'

// 注册
export const reqRegister = (username, password, name) => ajax('/users/userRegister', {username, password, name}, 'POST')

// 登录
export const reqLogin = (username, password) => ajax('/users/userLogin', {username, password}, 'POST')

// 获取店铺列表
export const reqShoplist = () => ajax('/shop/shopitem', {})

// 获取分类数据
export const reqClassify = (shopid) => ajax('/shop/classifyitem', {shopid}, 'POST')

// 获取商品列表
export const reqItemlist = (shopid) => ajax('/shop/itemlist', {shopid}, 'POST')

// 获取评论数据
export const reqComment = (shopid) => ajax('/shop/comment', {shopid}, 'POST')